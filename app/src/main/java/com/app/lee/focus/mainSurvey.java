package com.app.lee.focus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class mainSurvey extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_survey);

        SharedPreferences prefs = getSharedPreferences("Survey", MODE_PRIVATE);
        prefs.edit().putBoolean("surveyFlag", false).apply();
    }

    public void startSurvey(View view) {
        Intent intent = new Intent(getApplicationContext(), Tasks.class);
        startActivity(intent);
        finish();
    }
}
