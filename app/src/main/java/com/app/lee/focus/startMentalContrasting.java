package com.app.lee.focus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class startMentalContrasting extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_mental_contrasting);
    }

    public void startMC(View view){
        startActivity(new Intent(getApplicationContext(), MentalContrasting.class));
    }
}
