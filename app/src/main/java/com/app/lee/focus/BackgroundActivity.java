package com.app.lee.focus;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundActivity extends AsyncTask<String, Void, String> {

    //Required variables
    Context context;
    AlertDialog.Builder ad;

    BackgroundActivity(Context ctx){
        context = ctx;
    }

    @Override
    protected void onPreExecute() {
        ad = new AlertDialog.Builder(context);
        ad.setTitle("Login Info");
        ad.setCancelable(false);
    }

    @Override
    protected String doInBackground(String... params) {

        String username = params[0];
        String password = params[1];
        String method = params[2];

        String eUser, ePass;
        try {

            if(method.equals("register")){
                String link = "http://sqlphp.ddns.net/regpro.php";
                URL url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                //encoding
                eUser = encoder("user", username);
                ePass = encoder("pass", password);

                //transmission
                bw.write(eUser);
                bw.flush();
                bw.write(ePass);
                bw.flush();
                bw.close();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is,"iso-8859-1"));
                String response = "";
                String line = "";
                while ((line = br.readLine()) != null){
                    response += line;
                }

                br.close();is.close();httpURLConnection.disconnect();
                return response;
            }

            else if(method.equals("login")){
                String link = "http://sqlphp.ddns.net/login.php";
                URL url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                //encoding
                eUser = encoder("user", username);
                ePass = encoder("pass", password);

                //transmission
                bw.write(eUser);
                bw.flush();
                bw.write(ePass);
                bw.flush();
                bw.close();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is,"iso-8859-1"));
                String response = "";
                String line = "";
                while ((line = br.readLine()) != null){
                    response += line;
                }

                br.close();is.close();httpURLConnection.disconnect();

                return response;
            }

            else if(method.equals("test")){
                String link = "http://sqlphp.ddns.net/testecho.php";
                URL url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is,"iso-8859-1"));
                String response = "";
                String line = "";
                while ((line = br.readLine()) != null){
                    response += line;
                }

                br.close();is.close();httpURLConnection.disconnect();
                return response;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "null";
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(String result) {
        if(result == "Login Success" || result == "User created successfully" ) {
            Toast.makeText(context, result, Toast.LENGTH_SHORT).show();
        }
        else{
            ad.setMessage(result);
            ad.show();
        }
    }

    private String encoder(String type,String rawdata){
        String data = null;
        try {
            data = URLEncoder.encode(type, "UTF-8") + "=" + URLEncoder.encode(rawdata, "UTF-8") + "&";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }
}
