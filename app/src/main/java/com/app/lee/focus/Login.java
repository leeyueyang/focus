package com.app.lee.focus;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class Login extends AppCompatActivity {

    SharedPreferences prefs;
    String temp = "empty";
    TextView nameView;


    private void securelogin(String user){
        prefs = getSharedPreferences("MyApp", MODE_PRIVATE);
        prefs.edit().putString("username", user).commit();
    }

    private void setup(){
        prefs = getSharedPreferences("MyApp", MODE_PRIVATE);
        temp = prefs.getString("username", "empty");
        if (temp != "empty"){
            nameView.setText(temp);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        nameView = (TextView) findViewById(R.id.nameText);

        prefs = getSharedPreferences("MyApp", MODE_PRIVATE);
        if(prefs.contains("username")){
            Toast.makeText(Login.this,"not empty", Toast.LENGTH_SHORT);
            setup();
        }

    }

    public void goToMain(View view) {
        String nameText = nameView.getText().toString();

        TextView passwordView = (TextView) findViewById(R.id.passwordText);
        String passwordText = passwordView.getText().toString();

        if (!isNetworkAvailable()){
            Toast.makeText(this, "Internet connection not available", Toast.LENGTH_SHORT).show();
        }
        else if (nameText.isEmpty() || passwordText.isEmpty()) {
            return;
        }
        else if (passwordText.length() < 6){
            Toast.makeText(this, "Please have a pin at least 6 digits long", Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            connectServer(nameText,passwordText, "login");
            securelogin(nameText);
        }
    }

    public void register(View view){
        String nameText = nameView.getText().toString();

        TextView passwordView = (TextView) findViewById(R.id.passwordText);
        String passwordText = passwordView.getText().toString();

        if (!isNetworkAvailable()){
            Toast.makeText(this, "Internet connection not available", Toast.LENGTH_SHORT).show();
        }
        else if (nameText.isEmpty() || passwordText.isEmpty()) {
            return;
        } else {
            securelogin(nameText);
            connectServer(nameText,passwordText, "register");
        }
    }

    private void connectServer(String user, String pass, String method){
        securelogin(user);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        String result = logIn(user, pass, method);
        onPostExecute(result);
    }

    protected String logIn(String... params) {

        String username = params[0];
        String password = params[1];
        String method = params[2];

        String eUser, ePass;
        try {

            if (method.equals("register")) {
                String link = "http://sqlphp.ddns.net/regpro.php";
                URL url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                //encoding
                eUser = encoder("user", username);
                ePass = encoder("pass", password);

                //transmission
                bw.write(eUser);
                bw.flush();
                bw.write(ePass);
                bw.flush();
                bw.close();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
                String response = "";
                String line = "";
                while ((line = br.readLine()) != null) {
                    response += line;
                }

                br.close();
                is.close();
                httpURLConnection.disconnect();
                return response;
            }
            else if (method.equals("login")) {
                String link = "http://sqlphp.ddns.net/login.php";
                URL url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);

                OutputStream os = httpURLConnection.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                //encoding
                eUser = encoder("user", username);
                ePass = encoder("pass", password);

                //transmission
                bw.write(eUser);
                bw.flush();
                bw.write(ePass);
                bw.flush();
                bw.close();
                os.close();

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
                String response = "";
                String line = "";
                while ((line = br.readLine()) != null) {
                    response += line;
                }

                br.close();
                is.close();
                httpURLConnection.disconnect();

                return response;
            }
            else if (method.equals("test")) {
                String link = "http://sqlphp.ddns.net/testecho.php";
                URL url = new URL(link);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setDoInput(true);

                InputStream is = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
                String response = "";
                String line = "";
                while ((line = br.readLine()) != null) {
                    response += line;
                }

                br.close();
                is.close();
                httpURLConnection.disconnect();
                return response;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "null";
    }

    private String encoder(String type, String rawdata) {
        String data = null;
        try {
            data = URLEncoder.encode(type, "UTF-8") + "=" + URLEncoder.encode(rawdata, "UTF-8") + "&";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }

    protected void onPostExecute(String result) {

        if (result.contentEquals("Login Success")) {
            startActivity(new Intent(getApplicationContext(), redirect.class));
            //Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
            finish();
        }
        else if (result.contentEquals("User created successfully")){
            dialogPrompt();
        }
        else {
            Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
        }
    }

    private void dialogPrompt(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);

        builder.setTitle("Focus");
        builder.setMessage("Please enable the settings here and choose your DAs afterwards");
        builder.setCancelable(false);

        builder.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //return to application
                        Intent intent = new Intent(getApplicationContext(), Settings.class);
                        finish();
                        startActivity(intent);
                    }
                });

        android.app.AlertDialog alert = builder.create();

        alert.show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
