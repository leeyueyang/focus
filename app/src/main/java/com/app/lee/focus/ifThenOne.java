package com.app.lee.focus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class ifThenOne extends AppCompatActivity {

    //global variables
    char[] inputbuffer = new char[150];
    String ifThen1 = "iT1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_if_then_one);
        setup();
        try {
            autoUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void autoUpdate() throws IOException {
        TextView ob1 = (TextView) findViewById(R.id.obstacleTextOne);
        ob1.setText(reader("Ob1"));
    }

    public void nextButtonOne(View view){
        dialogPrompt();
    }

    private void dialogPrompt(){

        //creates dialog box
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

        builder1.setMessage("Would you like to save your plan?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        if(findViewString(R.id.ifThenEditOne).isEmpty()){
                            Toast.makeText(ifThenOne.this, "Please fill in the blank", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            try {
                                saveData(findViewString(R.id.ifThenEditOne), ifThen1);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            startActivity(new Intent(getApplicationContext(), ifThenTwo.class));
                            finish();
                        }
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.show();
    }

    private String reader(String filename) throws IOException {

        FileInputStream fis = openFileInput(filename);
        InputStreamReader reader = new InputStreamReader(fis);
        reader.read(inputbuffer);
        String data = new String(inputbuffer);
        clearBuffer(data);
        return data;

    }

    private void clearBuffer(String data){
        Integer length, i;
        length = data.length();
        for ( i=0; i<length; i++ ){inputbuffer[i] = ' ';}
    }

    private String findViewString(int ID){
        TextView ifThenText = (TextView) findViewById(ID);
        String ifThen = ifThenText.getText().toString();
        return ifThen;
    }

    private void saveData(String data, String filename) throws IOException {
        OutputStreamWriter out = new OutputStreamWriter(openFileOutput(filename, MODE_PRIVATE));
        out.write(data);
        out.flush();
        out.close();
    }

    private void setup(){
        EditText addCourseText = (EditText) findViewById(R.id.ifThenEditOne);
        addCourseText.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {

                        case KeyEvent.KEYCODE_ENTER:
                            dialogPrompt();
                            return true;
                        default:
                            break;

                    }
                }
                return false;
            }
        });

    }

}
