package com.app.lee.focus;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import static com.app.lee.focus.R.id.a1Text;
import static com.app.lee.focus.R.id.a2Text;
import static com.app.lee.focus.R.id.a3Text;
import static com.app.lee.focus.R.id.exitButton;

public class MentalContrasting extends AppCompatActivity {
    char[] inputbuffer = new char[150];

    private void autoUpdate() throws IOException {
        FileInputStream fis = openFileInput("aspectsStorage1");
        InputStreamReader reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        String data = new String(inputbuffer);

        TextView as1 = (TextView) findViewById(R.id.aspectOne);
        TextView as2 = (TextView) findViewById(R.id.aspectTwo);
        TextView as3 = (TextView) findViewById(R.id.aspectThree);
        as1.setText(data);
        clearBuffer(data);

        fis = openFileInput("aspectsStorage2");
        reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        data = new String(inputbuffer);
        as2.setText(data);
        clearBuffer(data);

        fis = openFileInput("aspectsStorage3");
        reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        data = new String(inputbuffer);
        as3.setText(data);
        clearBuffer(data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mental_contrasting);
        try {
            autoUpdate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void returnToMain(View view){
        startActivity(new Intent(MentalContrasting.this, MainActivity.class));
        finish();
    }

    public void editAspects(View view){
        startActivity(new Intent(MentalContrasting.this, Aspects.class));
        finish();
    }

    public void mcDialog() throws IOException {
        final WindowManager manager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.alpha = 1.0f;
        layoutParams.packageName = getPackageName();
        layoutParams.buttonBrightness = 1f;
        layoutParams.windowAnimations = android.R.style.Animation_Dialog;

        final View view = View.inflate(getApplicationContext(),R.layout.mc_main, null);
        TextView aspect1 = (TextView) view.findViewById(a1Text);
        TextView aspect2 = (TextView) view.findViewById(a2Text);
        TextView aspect3 = (TextView) view.findViewById(a3Text);

        Button eButton = (Button) view.findViewById(exitButton);

        char[] inputbuffer = new char[2500];
        FileInputStream fis = openFileInput("aspectsStorage1");
        InputStreamReader reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        String data = new String(inputbuffer);
        aspect1.setText(data);

        fis = openFileInput("aspectsStorage2");
        reader = new InputStreamReader(fis);


        reader.read(inputbuffer);
        data = new String(inputbuffer);
        inputbuffer = null;
        aspect2.setText(data);

        fis = openFileInput("aspectsStorage3");
        reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        data = new String(inputbuffer);
        aspect3.setText(data);


        eButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                manager.removeView(view);
            }
        });
        manager.addView(view, layoutParams);
    }

    private void clearBuffer(String data){
        Integer length, i;
        length = data.length();
        for ( i=0; i<length; i++ ){inputbuffer[i] = ' ';}
    }
}
