package com.app.lee.focus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class Tasks extends AppCompatActivity {

    String[] tasks = new String[10];
    Integer[] taskID = {R.id.taskEditOne, R.id.taskEditTwo, R.id.taskEditThree, R.id.taskEditFour, R.id.taskEditFive, R.id.taskEditSix, R.id.taskEditSeven, R.id.taskEditEight, R.id.taskEditNine, R.id.taskEditTen};
    String[] filename = {"t1","t2","t3","t4","t5","t6","t7","t8","t9","t10"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gateKeeper() == false){
                    Toast.makeText(Tasks.this, "Fill in 3 please", Toast.LENGTH_SHORT).show();
                }
                else {
                    dialogPrompt();
                }
            }
        });


        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void dialogPrompt(){

        //creates dialog box
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

        builder1.setMessage("Would you like to save these tasks?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        try {
                            taskScanner();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        startActivity(new Intent(getApplicationContext(), Obstacles.class));
                        finish();
                        dialog.cancel();
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.show();
    }

    private String findViewString(int ID){
        TextView obstacleText = (TextView) findViewById(ID);
        String obstacle = obstacleText.getText().toString();
        return obstacle;
    }

    private void saveData(String data, String filename) throws IOException {
        OutputStreamWriter out = new OutputStreamWriter(openFileOutput(filename, MODE_PRIVATE));
        out.write(data);
        out.flush();
        out.close();
    }

    private boolean gateKeeper(){

        for (Integer i = 0; i < 3; i++){
            if(findViewString(taskID[i]).isEmpty()) {
                return false;
            }
        }

        return true;
    }

    private void taskScanner() throws IOException {
        for (Integer i = 0; i < 10; i++){
            if(findViewString(taskID[i]).isEmpty()) {
                tasks[i] = "NIL";
            }
            else{
                tasks[i] = findViewString(taskID[i]);
            }
        }

        for (Integer i = 0; i < 10; i++){
            saveData(tasks[i], filename[i]);
        }
    }

}

