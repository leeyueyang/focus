package com.app.lee.focus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.OutputStreamWriter;

public class Aspects extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aspects);
    }

    public void storeStart(View view){
        String FILENAME1 = "aspectsStorage1";
        String FILENAME2 = "aspectsStorage2";
        String FILENAME3 = "aspectsStorage3";
        String aspect1, aspect2, aspect3;

        TextView aspectOneText = (TextView) findViewById(R.id.aspectOneText);
        aspect1 = aspectOneText.getText().toString();

        TextView aspectTwoText = (TextView) findViewById(R.id.aspectTwoText);
        aspect2 = aspectTwoText.getText().toString();

        TextView aspectThreeText = (TextView) findViewById(R.id.aspectThreeText);
        aspect3 = aspectThreeText.getText().toString();

        try {
            if(aspect1.isEmpty() || aspect2.isEmpty() || aspect3.isEmpty()) {
                Toast.makeText(this, "Please fill up the blanks", Toast.LENGTH_SHORT).show();
            }
            else {

                OutputStreamWriter out1 = new OutputStreamWriter(openFileOutput(FILENAME1, MODE_PRIVATE));
                out1.write(aspect1);
                out1.flush();
                out1.close();

                OutputStreamWriter out2 = new OutputStreamWriter(openFileOutput(FILENAME2, MODE_PRIVATE));
                out2.write(aspect2);
                out2.flush();
                out2.close();

                OutputStreamWriter out3 = new OutputStreamWriter(openFileOutput(FILENAME3, MODE_PRIVATE));
                out3.write(aspect3);
                out3.flush();
                out3.close();

                Toast.makeText(this, "Your aspects have been saved", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Aspects.this, MentalContrasting.class));
            }
        } catch (Throwable t) {

            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();

        }
    }

    public void returnToMain(View view){
        startActivity(new Intent(Aspects.this, MentalContrasting.class));
    }

    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }

}
