package com.app.lee.focus;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.app.AlertDialog;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rvalerio.fgchecker.AppChecker;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import static com.app.lee.focus.R.id.a1Text;
import static com.app.lee.focus.R.id.a2Text;
import static com.app.lee.focus.R.id.a3Text;
import static com.app.lee.focus.R.id.exitButton;

public class MainActivity extends AppCompatActivity {

    //global variables
        int hour, mins, totaltime;
        EditText eHours, eMins;
        int pidNo;
        boolean dialogFlag, mcAlertDialogFlag, daFlag;
        String[] DAs;

    //polling
        String oldPackageName, packageName;

    //for timer
        ProgressDialog barProgressDialog;
        final launchBarDialog launchDialog = new launchBarDialog();

    //username manager
        SharedPreferences prefs;
        private String username = "";

    //dialogs
    AlertDialog mcAlertDialog;
    WindowManager manager1;
    View view1;

    //for reading
    char[] inputbuffer = new char[150];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        eHours = (EditText)findViewById(R.id.editHour);
        eMins = (EditText)findViewById(R.id.editMinutes);

        prefs = getSharedPreferences("MyApp", MODE_PRIVATE);
        username = prefs.getString("username", "UNKNOWN");
        TextView userView = (TextView)findViewById(R.id.usernameTextView);
        userView.setText("Welcome " + username + "!");
    }

    public void dialogPrompt(View view) throws IOException {


        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
             {InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
             }


        //calculates time
        if (eHours.getText().toString().length() == 0) {hour = 0;}
        else {hour = Integer.parseInt(eHours.getText().toString());}

        if (eMins.getText().toString().length() == 0) {mins = 0;}
        else {mins = Integer.parseInt(eMins.getText().toString());}

        totaltime = (hour * 60 * 60 * 1000) + (mins * 60 * 1000);

        if (totaltime == 0){
            Toast.makeText(MainActivity.this,"Please fill in the blanks", Toast.LENGTH_SHORT).show();
        }
        else if (totaltime > 86400000 || totaltime < 0){
            Toast.makeText(MainActivity.this,"Please enter a valid timing", Toast.LENGTH_SHORT).show();
        }
        else if(!gatekeeper()){
            Toast.makeText(this, "DAs have not been set", Toast.LENGTH_SHORT).show();
        }
        else if(!isNetworkAvailable()) {
            Toast.makeText(this, "No Internet Connection.. Please be connected to the Internet", Toast.LENGTH_LONG).show();
        }
        else {

            if (!isServiceRunning(countDownService.class)) {
                   try{
                    reader("t1");

                        //creates dialog box
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

                        builder1.setMessage("Would you like to begin your session now?");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();

                                        //display time at bottom left in ms
                                        Intent i = new Intent(getApplicationContext(), countDownService.class);
                                        i.putExtra("time", totaltime);
                                        i.putExtra("username", username);
                                        i.putExtra("DAs", DAs);

                                        startService(i);

                                    }
                                });

                        builder1.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        builder1.show();
                    }
                   catch (IOException e) {
                    Toast.makeText(this, "Start of day survey not completed", Toast.LENGTH_SHORT).show();
                    }
            }
            else{
                Toast.makeText(this, "Session has already started", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void debugStart(View view){

        startActivity(new Intent(getApplicationContext(),DistractionAppsPicker.class));

        //daFlag = true;

        // /DAs = dap.calc();
        /*
        totaltime = 5000;
        Intent i = new Intent(getApplicationContext(), countDownService.class);
        i.putExtra("time", totaltime);

        startService(i);
        */

    }

    public void logOutButton(View view){

        if(isServiceRunning(countDownService.class)){
            Toast.makeText(this, "Session still in progress!", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(MainActivity.this, "You have been logged out", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(MainActivity.this, Login.class));
            this.finish();
        }
    }

    public void viewLaunch(View view){
        startActivity(new Intent(MainActivity.this, MentalContrasting.class));
    }

    public void settingsLaunch(View view){
        startActivity(new Intent(MainActivity.this, com.app.lee.focus.Settings.class));
    }

    public void surveyLaunch(View view) throws IOException {
        if(!isNetworkAvailable()){
            Toast.makeText(this, "Internet not available", Toast.LENGTH_SHORT).show();
        }
        else if(!surveyChecker(username)){
            Toast.makeText(this, "Day has already ended", Toast.LENGTH_SHORT).show();
        }
        else if(surveyChecker(username) && !surveyGatekeeper()){
            startActivity(new Intent(getApplicationContext(), mainSurvey.class));
        }
        else if(surveyGatekeeper()){
            Toast.makeText(this, "Survey already completed", Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(this, "Survey Verification Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void stopTimer(View view){

        if (isServiceRunning(countDownService.class)) {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

            builder1.setMessage("Are you sure you would like to end your session?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                            //end timer
                            Intent i = new Intent(getApplicationContext(), countDownService.class);
                            stopService(i);
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            builder1.show();
        }

        else{
            Toast.makeText(this, "Session is not currently not started", Toast.LENGTH_SHORT).show();
        }
    }

    public void endSurvey(View view) throws IOException {
        if(!isServiceRunning(countDownService.class)) {
            startActivity(new Intent(getApplicationContext(), endSurvey.class));
        }
        else{
            Toast.makeText(this, "Session still in progress!", Toast.LENGTH_SHORT).show();
        }
    }

    private void mcDialog() throws IOException {
        final WindowManager manager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.alpha = 1.0f;
        layoutParams.packageName = getPackageName();
        layoutParams.buttonBrightness = 1f;
        layoutParams.windowAnimations = android.R.style.Animation_Dialog;

        final View view = View.inflate(getApplicationContext(),R.layout.mc_main, null);
        TextView aspect1 = (TextView) view.findViewById(a1Text);
        TextView aspect2 = (TextView) view.findViewById(a2Text);
        TextView aspect3 = (TextView) view.findViewById(a3Text);

        Button eButton = (Button) view.findViewById(exitButton);

        char[] inputbuffer = new char[32];
        FileInputStream fis = openFileInput("aspectsStorage1");
        InputStreamReader reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        String data = new String(inputbuffer);
        aspect1.setText(data);

        fis = openFileInput("aspectsStorage2");
        reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        data = new String(inputbuffer);
        aspect2.setText(data);

        fis = openFileInput("aspectsStorage3");
        reader = new InputStreamReader(fis);

        reader.read(inputbuffer);
        data = new String(inputbuffer);
        aspect3.setText(data);


        eButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                /*
                Intent startMain = new Intent(Intent.ACTION_MAIN);
                startMain.addCategory(Intent.CATEGORY_HOME);
                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(startMain);
                */

                AppChecker appChecker = new AppChecker();
                String pkgName = appChecker.getForegroundApp(getApplicationContext());
                Boolean bool=false;
                //Toast.makeText(MainActivity.this, pkgName, Toast.LENGTH_SHORT).show();
                if(pkgName.contains("launcher")|pkgName.contains("home")){
                    bool = true;
                }

                if(bool) {
                    manager.removeView(view);
                }
                //Toast.makeText(MainActivity.this, String.valueOf(pidFinder()), Toast.LENGTH_SHORT).show();
            }
        });
        manager.addView(view, layoutParams);


    }

    private class launchBarDialog {

        private void constructor() {
            barProgressDialog = new ProgressDialog(getApplicationContext());

            barProgressDialog.setTitle("Focus");
            barProgressDialog.setMessage("Session is in progress");
            barProgressDialog.setIndeterminate(true);
            barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
            barProgressDialog.setProgress(0);
            barProgressDialog.setCancelable(false);
            barProgressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "End Session", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getApplicationContext(),"Your session has ended", Toast.LENGTH_SHORT).show();

                    //stop timer here
                    //timer.stop();

                    dialog.dismiss();

                }
            });
            barProgressDialog.show();
        }

        private void close(){barProgressDialog.cancel();}
    }

    private boolean isServiceRunning(Class<?> serviceClass){
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : am.getRunningServices(Integer.MAX_VALUE)){
            if (serviceClass.getName().equals(service.service.getClassName())){
                return true;
            }
        }
        return false;
    }

    private String reader(String filename) throws IOException {

        FileInputStream fis = openFileInput(filename);
        InputStreamReader reader = new InputStreamReader(fis);
        reader.read(inputbuffer);
        String data = new String(inputbuffer);
        clearBuffer(data);
        return data;

    }

    private void clearBuffer(String data){
        Integer length, i;
        length = data.length();
        for ( i=0; i<length; i++ ){inputbuffer[i] = ' ';}
    }

    private boolean gatekeeper(){
        Boolean logic = false;
        SharedPreferences prefs = getSharedPreferences("DAs", MODE_PRIVATE);
        logic = prefs.getBoolean("daCompleteFlag",false);
        
        return logic;
    }

    private boolean surveyGatekeeper(){
        Boolean logic = false;
        SharedPreferences prefs = getSharedPreferences("Survey", MODE_PRIVATE);
        logic = prefs.getBoolean("surveyFlag",false);

        return logic;
    }

    private boolean surveyChecker(String user) throws IOException {
        Boolean logic = false;
        String link = "http://sqlphp.ddns.net/checker.php";
        URL url = new URL(link);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setDoInput(true);

        OutputStream os = httpURLConnection.getOutputStream();
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

        //Toast.makeText(this, user, Toast.LENGTH_SHORT).show();

        String eUser = encoder("user", user);

        bw.write(eUser);
        bw.flush();
        bw.close();
        os.close();

        InputStream is = httpURLConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
        String response = "";
        String line = "";
        while ((line = br.readLine()) != null) {
            response += line;
        }

        //Toast.makeText(this, response, Toast.LENGTH_SHORT).show();

        if (response.contentEquals("OK")){
            logic = true;
        }

        br.close();
        is.close();
        httpURLConnection.disconnect();

        return logic;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private String encoder(String type, String rawdata) {
        String data = null;
        try {
            data = URLEncoder.encode(type, "UTF-8") + "=" + URLEncoder.encode(rawdata, "UTF-8") + "&";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }
}

