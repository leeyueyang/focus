package com.app.lee.focus;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.List;

public class DistractionAppsPicker extends AppCompatActivity {

    ListView listView ;
    int installedAppsCounter = 0;
    List<ApplicationInfo> packages;
    String[] pkgName;
    DAAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distraction_apps_picker);

        PackageManager pm = getPackageManager();
        packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);

        cycler();

        listView = (ListView) findViewById(R.id.daListView);

        display();
    }

    private void cycler(){

        for (ApplicationInfo packageInfo : packages){
            if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                installedAppsCounter++;
            }
        }

        pkgName = new String[installedAppsCounter];

        int i = 0;
        for (ApplicationInfo packageInfo : packages){
            if ((packageInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                pkgName[i] = packageInfo.packageName;
                i++;
            }
        }

    }

    private void display(){

        adapter = new DAAdapter(this,
                R.layout.da_row, R.id.daPlaceholder, pkgName, pkgName.length);
        listView.setAdapter(adapter);
    }

    public void calculate(View view){
        int number = adapter.returnValue();
        /*
        for(int i = 0; i < results.length; i++) {
            if(!results[i].contentEquals("")) {
                Toast.makeText(this, results[i], Toast.LENGTH_SHORT).show();
            }
        }*/

        /*
        SharedPreferences prefs;

        //Cleaner

        getApplicationContext().getSharedPreferences("DAs", 0).edit().clear().commit();

        prefs = getSharedPreferences("DAs", MODE_PRIVATE);
        prefs.edit().putString("daCounter",String.valueOf(number)).commit();

        String[] pkgName = adapter.pkgName();

        for(int i = 1; i<pkgName.length; i++){
            if(!pkgName[i].contentEquals("")) {
                prefs = getSharedPreferences("DAs", MODE_PRIVATE);
                prefs.edit().putString("da" + i, pkgName[i]).commit();
            }
        }

        prefs.edit().putBoolean("daCompleteFlag",true).commit(); */

        String[] pkgName = adapter.pkgName();
        Intent i = new Intent(getApplicationContext(),DAConfirm.class).putExtra("cfmPkg",pkgName);
        i.putExtra("number",number);
        startActivity(i);
    }
}
