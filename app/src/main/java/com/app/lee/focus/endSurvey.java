package com.app.lee.focus;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class endSurvey extends AppCompatActivity {

    char[] inputbuffer = new char[150];
    Integer[] taskID = {R.id.task1Text, R.id.task2Text, R.id.task3Text, R.id.task4Text, R.id.task5Text, R.id.task6Text, R.id.task7Text, R.id.task8Text, R.id.task9Text, R.id.task10Text};
    Integer[] checkTaskID = {R.id.checkBox1, R.id.checkBox2, R.id.checkBox3, R.id.checkBox4, R.id.checkBox5, R.id.checkBox6, R.id.checkBox7, R.id.checkBox8, R.id.checkBox9, R.id.checkBox10};
    String[] filename = {"t1","t2","t3","t4","t5","t6","t7","t8","t9","t10"};
    Boolean[] logic = {true, true, true, true, true, true, true, true, true, true};
    Integer veriedCounter = 0;
    Integer logicCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_survey);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("End of Day Survey");

        try {
            autoUpdate();
        } catch (IOException e) {
            Toast.makeText(this, "Start of Day Survey not found", Toast.LENGTH_SHORT).show();
            finish();
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPrompt();
            }
        });

    }

    private void autoUpdate() throws IOException {

        for(int i = 0; i < filename.length; i++){
            TextView tX = (TextView) findViewById(taskID[i]);
            tX.setText(reader(filename[i]));
        }
    }

    private String reader(String filename) throws IOException {

        FileInputStream fis = openFileInput(filename);
        InputStreamReader reader = new InputStreamReader(fis);
        reader.read(inputbuffer);
        String data = new String(inputbuffer);
        clearBuffer(data);
        return data;

    }

    private void clearBuffer(String data){
        Integer length, i;
        length = data.length();
        for ( i=0; i<length; i++ ){inputbuffer[i] = ' ';}
    }

    private void endButton(){
        try {
            gateKeeper();
        } catch (IOException e) {
            Toast.makeText(this, "Error Filtering Checkboxes", Toast.LENGTH_SHORT).show();
        }
        upload(String.valueOf(logicCounter), String.valueOf(veriedCounter) );
        springCleaning();
        startActivity(new Intent(getApplicationContext(), redirect.class));
        finish();
    }

    private void dialogPrompt(){

        //creates dialog box
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

        builder1.setMessage("Would you like to save your response?");
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(!isNetworkAvailable()){
                            Toast.makeText(endSurvey.this, "No Internet Connection.. Please be connected to the Internet", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(endSurvey.this, "Thank you for participating! The survey has ended", Toast.LENGTH_SHORT).show();
                            endButton();

                            SharedPreferences prefs = getSharedPreferences("Survey", MODE_PRIVATE);
                            prefs.edit().putBoolean("surveyFlag", false).apply();

                            finish();
                        }
                    }
                });

        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        builder1.show();
    }

    private void gateKeeper() throws IOException {
        for(int i = 0; i < filename.length; i++) {
            if (reader(filename[i]).startsWith("NIL")) {
                // this will detect which fields have actual tasks
                logic[i] = false;
            }
        }

        for(int i = 0; i < filename.length; i++) {
            CheckBox checkBox = (CheckBox) findViewById(checkTaskID[i]);
            if (checkBox.isChecked() && logic[i]) {
                veriedCounter++;
            }
        }

        for(int i = 0; i < logic.length; i++){
            if (logic[i]){
                logicCounter++;
            }
        }
    }

    private String encoder(String type, String rawdata) {
        String data = null;
        try {
            data = URLEncoder.encode(type, "UTF-8") + "=" + URLEncoder.encode(rawdata, "UTF-8") + "&";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }

    private String upload(String... params) {

        String data1 = params[0];
        String data2 = params[1];

        SharedPreferences prefs = getSharedPreferences("MyApp", MODE_PRIVATE);
        String data3 = prefs.getString("username", "UNKNOWN");

        String eTasks, eCompletedTasks, eUser;
        try {
            String link = "http://sqlphp.ddns.net/endsurvey.php";
            URL url = new URL(link);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);

            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            //encoding
            eTasks = encoder("notasks", data1);
            eCompletedTasks = encoder("nocompletedtasks", data2);
            eUser = encoder("user",data3);

            //transmission
            bw.write(eTasks);
            bw.flush();
            bw.write(eCompletedTasks);
            bw.flush();
            bw.write(eUser);
            bw.flush();
            bw.close();
            os.close();

            InputStream is = httpURLConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
            String response = "";
            String line = "";
            while ((line = br.readLine()) != null) {
                response += line;
            }

            br.close();
            is.close();
            httpURLConnection.disconnect();
            return response;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "null";
    }

    private void springCleaning(){
        String[] obFiles = {"Ob1", "Ob2", "Ob3"};
        String[] itFiles = {"iT1", "iT2", "iT3"};

        for(int i = 0; i < filename.length; i++) {
            deleteFile(filename[i]);
        }
        for(int i = 0; i < obFiles.length; i++){
            deleteFile(obFiles[i]);
        }
        for(int i = 0; i < itFiles.length; i++){
            deleteFile(itFiles[i]);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
