package com.app.lee.focus;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


/**
 * Created by leeyu on 11/8/2016.
 */

public class DACfmAdapter extends ArrayAdapter{


    public DACfmAdapter(Context context, int resource, int view, String[] value, int size){
        super(context,resource, view, value);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final View row_layout_view = super.getView(position, convertView, parent);
        final TextView tv = (TextView) row_layout_view.findViewById(R.id.daPlaceholder);

        return row_layout_view;
    }


}
