package com.app.lee.focus;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by leeyu on 11/8/2016.
 */

public class DAAdapter extends ArrayAdapter{

    int checkedCounter=0;
    String[] pkgNames;
    int maxSize=0;
    boolean[] highlighted;

    public DAAdapter(Context context, int resource, int view, String[] value, int size){
        super(context,resource, view, value);
        maxSize = size;
        highlighted = new boolean[maxSize];
        pkgNames = new String[maxSize];
        for (int i = 0; i < maxSize; i++){
            pkgNames[i] = "";
            highlighted[i] = false;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final View row_layout_view = super.getView(position, convertView, parent);

        final boolean[] gatekeeper = {false};
        final TextView tv = (TextView) row_layout_view.findViewById(R.id.daPlaceholder);

        for(int i = 0; i < highlighted.length; i++) {
            if (highlighted[i]){
                tv.setBackgroundColor(Color.parseColor("#414e98"));
            }
            else{
                tv.setBackgroundColor(Color.parseColor("#5564b7"));
            }
        }
        row_layout_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for(int i = 0; i < maxSize; i++){
                    //handles the removal of the application

                    if (pkgNames[i].contentEquals(tv.getText().toString())){
                        pkgNames[i] = "";
                        Toast.makeText(getContext(), tv.getText().toString() + " has been deselected", Toast.LENGTH_SHORT).show();
                        //row_layout_view.setBackgroundColor(Color.parseColor("#5564b7"));  //set color back to default
                        //highlighted[position] = false;                                    //sets the box to being unchecked
                        //notifyDataSetChanged();                                           //redraw the list
                        checkedCounter--;
                        i = maxSize;
                        gatekeeper[0]=true;
                    }
                    else {
                        gatekeeper[0]=false;
                    }
                }
                if(!gatekeeper[0]) {
                    Toast.makeText(getContext(), tv.getText().toString() + " has been selected", Toast.LENGTH_SHORT).show();
                    checkedCounter++;

                    //row_layout_view.setBackgroundColor(Color.parseColor("#414e98")); //set color to selected
                    //highlighted[position] = true;                       //set the box to being checked
                    //notifyDataSetChanged();                             //redraw the data set
                    pkgNames[checkedCounter] = tv.getText().toString();
                }
            }
        });
        return row_layout_view;
    }

    public int returnValue(){

        return checkedCounter;
    }

    public String[] pkgName(){
        return pkgNames;
    }


}
