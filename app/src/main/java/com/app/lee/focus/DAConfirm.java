package com.app.lee.focus;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class DAConfirm extends AppCompatActivity {

    ListView listView;
    DACfmAdapter adapter2;
    String[] pkgName, filteredPkgName;
    int size = 0;
    int number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daconfirm);
        Toast.makeText(this, "Please confirm your choices, if you have chosen something by accident, return to the previous screen and deselect it", Toast.LENGTH_LONG).show();
        loader();

        listView = (ListView) findViewById(R.id.daCfmListView);

        display();
    }

    private void display(){
        adapter2 = new DACfmAdapter(this,
                R.layout.da_row, R.id.daPlaceholder, filteredPkgName, size);
        listView.setAdapter(adapter2);
    }

    private void loader(){
        int filterCounter = 0;

        pkgName = getIntent().getStringArrayExtra("cfmPkg");
        number = getIntent().getIntExtra("number", 0);

        for(int i = 0; i<pkgName.length; i++){
            if(!pkgName[i].contentEquals("")) {
                size++;
            }
        }

        filteredPkgName = new String[size];

        for(int i = 0; i<pkgName.length; i++){
            if(!pkgName[i].contentEquals("")) {
                filteredPkgName[filterCounter] = pkgName[i];
                filterCounter++;
            }
        }
    }

    public void returntomain(View view){
        SharedPreferences prefs;


        //Cleaner
        getApplicationContext().getSharedPreferences("DAs", 0).edit().clear().commit();

        prefs = getSharedPreferences("DAs", MODE_PRIVATE);
        prefs.edit().putString("daCounter",String.valueOf(number)).commit();

        for(int i = 0; i<filteredPkgName.length; i++){  
            prefs = getSharedPreferences("DAs", MODE_PRIVATE);
            prefs.edit().putString("da" + i, filteredPkgName[i]).commit();
        }

        prefs.edit().putBoolean("daCompleteFlag",true).commit();
        Toast.makeText(this, "DAs saved", Toast.LENGTH_SHORT).show();


        finish();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }


}
