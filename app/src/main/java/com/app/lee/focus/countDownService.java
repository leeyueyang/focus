package com.app.lee.focus;

import android.app.AlertDialog;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.rvalerio.fgchecker.AppChecker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

import static com.app.lee.focus.R.id.conventionalButton;
import static com.app.lee.focus.R.id.fullTextView;
import static com.app.lee.focus.R.id.mcTV1;
import static com.app.lee.focus.R.id.mcservicebutton;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class countDownService extends IntentService {

    //MC processes
    String oldPackageName, packageName;
    AppChecker appChecker = new AppChecker();

    //Flags
    boolean timerFlag, mcAlertDialogFlag;

    //Timer
    timeCounter timer;

    //Debugging
    String  globalResponse;

    //Reporting data
    Integer totaltime, mcCounter, daCounter;
    String username, sessionID, remainingTime;
    String[] DAs;

    char[] inputbuffer = new char[150];

    //dialogs
    AlertDialog mcAlertDialog;

    public countDownService() {
        super("countDownService");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Countdown Service", "Intent has been created");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mcCounter = 0;
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

        Drawable bigIcon = getResources().getDrawable(R.drawable.ic_schedule_black_36dp);
        Bitmap bitmapIcon      = ((BitmapDrawable) bigIcon).getBitmap();

        Notification.Builder notification = new Notification.Builder(this)
                .setSmallIcon(R.drawable.ic_schedule_white_36dp)
                .setLargeIcon(bitmapIcon)
                .setContentTitle("Timer service")
                .setContentText("Your session has started")
                .setOngoing(true)
                .setTicker("Your Session is Starting")
                .setContentIntent(pendingIntent);

        //initiates the timer
        totaltime = intent.getIntExtra("time", -1);
        username = intent.getStringExtra("username");

        SharedPreferences prefs = getSharedPreferences("DAs", MODE_PRIVATE);
        daCounter = Integer.valueOf(prefs.getString("daCounter", "-1"));

        DAs = new String[daCounter];

        if(daCounter == -1){
            Toast.makeText(this, "DA handover error", Toast.LENGTH_SHORT).show();
        }
        for(int a=0; a<=(daCounter-1); a++){
            DAs[a] = prefs.getString("da"+a, "com.facebook.katana");
        }

        timer = new timeCounter();
        timer.constructor(intent);
        timer.start();
        Toast.makeText(this, "Session has started", Toast.LENGTH_SHORT).show();

        startForeground(101, notification.build());

        return START_NOT_STICKY;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("Countdown Service", "Intent has started");

    }

    public class timeCounter{
        CountDownTimer counter;

        public void constructor(Intent intent){


            if(totaltime == -1){
                Toast.makeText(countDownService.this, "There is an error", Toast.LENGTH_SHORT).show();
            }
            else {
                    //uploadToServer(username ,String.valueOf(totaltime));

                    asyncUpload uploader = new asyncUpload();
                    uploader.execute(username, String.valueOf(totaltime), "time", "", "");

                    counter = new CountDownTimer(totaltime, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            //checking for interception

                            packageName = appChecker.getForegroundApp(getApplicationContext());
                            if (packageName.equals(oldPackageName)) {
                            } else {
                                mentalContrasting(packageName);
                            }
                            oldPackageName = packageName;
                            remainingTime = String.valueOf(millisUntilFinished);
                        }

                        @Override
                        public void onFinish() {
                            timerFlag = false;

                            dialogRePrompt();
                            stopSelf();
                        }
                    };
            }
        }
        public void stop(){

            asyncUpload uploader = new asyncUpload();
            uploader.execute(sessionID, remainingTime, "end", "", "");
            counter.cancel();
            /*
            if(isNetworkAvailable()) {
                updateEnd(sessionID, remainingTime);
                counter.cancel();
            }
            else{
                try {
                    fullDialog();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }*/
            }
        public void start(){counter.start();}
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        timer.stop();
        Toast.makeText(this, "Session has ended", Toast.LENGTH_SHORT).show();
    }

    private void mentalContrasting(final String packageName){

        final String distractionApps[]= DAs;
        for (int i = 0; i < distractionApps.length; i++){

            if (distractionApps[i].toString().equals(packageName)){
                //removes functionality for kitkat
                //Toast.makeText(this,"detected!", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder3 = new AlertDialog.Builder(getApplicationContext());

                builder3.setTitle("Focus");
                builder3.setMessage("Would you like to end your session now?");
                builder3.setCancelable(false);

                builder3.setPositiveButton(
                        "Begin Mental Contrasting",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                    mcCounter++;
                                    //mcUpload(packageName, mcCounter.toString(), sessionID, username);

                                    asyncUpload uploader = new asyncUpload();
                                    uploader.execute(packageName, mcCounter.toString(), "mcUpload", sessionID, username);

                                    dialog.cancel();

                                    try {
                                        mcServiceDialog();
                                    } catch (IOException e) {
                                        Toast.makeText(countDownService.this, "Service Dialog Failed", Toast.LENGTH_SHORT).show();
                                    }
                            }
                        });

                builder3.setNegativeButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();

                                Toast.makeText(countDownService.this, "Your session has ended", Toast.LENGTH_SHORT).show();
                                stopSelf();
                            }
                        });

                mcAlertDialog = builder3.create();

                mcAlertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                mcAlertDialog.show(); mcAlertDialogFlag=true;

            }
        }
    }

    public void dialogRePrompt(){

        //creates dialog box
        //note that this dialog box needs to be global in future

        if(mcAlertDialogFlag){
            mcAlertDialog.dismiss();
        }
        AlertDialog.Builder builder2 = new AlertDialog.Builder(getApplicationContext());

        builder2.setTitle("Focus");
        builder2.setMessage("Your session has ended");
        builder2.setCancelable(false);

        builder2.setPositiveButton(
                "Return to App",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        //return to application
                        Intent intent = new Intent(getApplicationContext(), redirect.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });

        builder2.setNegativeButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        Toast.makeText(countDownService.this, "Thank you for participating!", Toast.LENGTH_SHORT).show();
                    }
                });

        AlertDialog alert = builder2.create();

        alert.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        alert.show();

    }

    private void updateEnd(String... params) {
        String data1 = params[0];
        String data2 = params[1];

        String eSessionID, eRemainingTime;
        try {
            String link = "http://sqlphp.ddns.net/updatetime.php";
            URL url = new URL(link);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);

            OutputStream os = httpURLConnection.getOutputStream();
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            //encoding
            eSessionID = encoder("sessionID", data1);
            eRemainingTime = encoder("remainingTime", data2);

            //transmission
            bw.write(eSessionID);
            bw.flush();
            bw.write(eRemainingTime);
            bw.flush();
            bw.close();
            os.close();

            InputStream is = httpURLConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
            String response = "";
            String line = "";
            while ((line = br.readLine()) != null) {
                response += line;
            }

            br.close();
            is.close();
            httpURLConnection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String encoder(String type, String rawdata) {
        String data = null;
        try {
            data = URLEncoder.encode(type, "UTF-8") + "=" + URLEncoder.encode(rawdata, "UTF-8") + "&";
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return data;
    }

    private void mcServiceDialog() throws IOException{
        final WindowManager manager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.alpha = 1.0f;
        layoutParams.packageName = getPackageName();
        layoutParams.buttonBrightness = 1f;
        layoutParams.windowAnimations = android.R.style.Animation_Dialog;

        final View view = View.inflate(getApplicationContext(),R.layout.mcservicedialog, null);

        Button eButton = (Button) view.findViewById(mcservicebutton);

        eButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), MentalContrasting.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                manager.removeView(view);
            }
        });
        manager.addView(view, layoutParams);
    }

    private void conventionalDialog(){
        final WindowManager manager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.alpha = 1.0f;
        layoutParams.packageName = getPackageName();
        layoutParams.buttonBrightness = 1f;
        layoutParams.windowAnimations = android.R.style.Animation_Dialog;
        layoutParams.setTitle("Focus");

        final View view = View.inflate(getApplicationContext(),R.layout.mcservicedialog, null);
        Button eButton = (Button) view.findViewById(conventionalButton);

        try {
            TextView tv1 = (TextView) view.findViewById(mcTV1);
            tv1.setText(reader("iT1"));
            TextView tv2 = (TextView) view.findViewById(mcTV1);
            tv2.setText(reader("iT12"));
            TextView tv3 = (TextView) view.findViewById(mcTV1);
            tv3.setText(reader("iT3"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        eButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                manager.removeView(view);
            }
        });
        manager.addView(view, layoutParams);
    }

    private String reader(String filename) throws IOException {

        FileInputStream fis = openFileInput(filename);
        InputStreamReader reader = new InputStreamReader(fis);
        reader.read(inputbuffer);
        String data = new String(inputbuffer);
        clearBuffer(data);
        return data;

    }

    private void clearBuffer(String data){
        Integer length, i;
        length = data.length();
        for ( i=0; i<length; i++ ){inputbuffer[i] = ' ';}
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void fullDialog() throws IOException{
        final WindowManager manager = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.alpha = 1.0f;
        layoutParams.packageName = getPackageName();
        layoutParams.buttonBrightness = 1f;
        layoutParams.windowAnimations = android.R.style.Animation_Dialog;

        final View view = View.inflate(getApplicationContext(),R.layout.mcservicedialog, null);

        Button eButton = (Button) view.findViewById(mcservicebutton);
        eButton.setText("Proceed");

        TextView tView = (TextView) view.findViewById(fullTextView);
        tView.setText("Your internet connection was not available.. Please ensure that you are connected");

        eButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    updateEnd(sessionID, remainingTime);
                    timer.stop();
                    manager.removeView(view);
                }
            }
        });
        manager.addView(view, layoutParams);
    }

    private class asyncUpload extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... data){

            String data1 = data[0];
            String data2 = data[1];
            String type = data[2];
            String data3 = data[3];
            String data4 = data[4];

            String eUser, eTime;
            String eSessionID, eRemainingTime;
            String eDA, eCounter;

            while(true) {
                if (isNetworkAvailable()) {
                    try {

                        String link = "";

                        if (type == "time") {
                            link = "http://sqlphp.ddns.net/datatime.php";

                            URL url = new URL(link);
                            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                            httpURLConnection.setRequestMethod("POST");
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setDoInput(true);

                            OutputStream os = httpURLConnection.getOutputStream();
                            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));


                            //encoding
                            eUser = encoder("user", data1);
                            eTime = encoder("time", data2);

                            //transmission
                            bw.write(eUser);
                            bw.flush();
                            bw.write(eTime);
                            bw.flush();
                            bw.close();
                            os.close();


                            InputStream is = httpURLConnection.getInputStream();
                            BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"));
                            String response = "";
                            String line = "";
                            while ((line = br.readLine()) != null) {
                                response += line;
                            }

                            sessionID = response;

                            br.close();
                            is.close();
                            httpURLConnection.disconnect();
                            return response;
                        } else if (type == "end") {
                            link = "http://sqlphp.ddns.net/updatetime.php";

                            //encoding
                            eSessionID = encoder("sessionID", data1);
                            eRemainingTime = encoder("remainingTime", data2);

                            URL url = new URL(link);
                            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                            httpURLConnection.setRequestMethod("POST");
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setDoInput(true);

                            OutputStream os = httpURLConnection.getOutputStream();
                            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                            //transmission
                            bw.write(eSessionID);
                            bw.flush();
                            bw.write(eRemainingTime);
                            bw.flush();
                            bw.close();
                            os.close();

                            InputStream is = httpURLConnection.getInputStream();
                            is.close();

                            httpURLConnection.disconnect();

                        } else if (type == "mcUpload") {
                            link = "http://sqlphp.ddns.net/updatemc.php";

                            //encoding
                            eDA = encoder("DA", data1);
                            eCounter = encoder("counter", data2);
                            eSessionID = encoder("sessionID", data3);
                            eUser = encoder("user", data4);

                            URL url = new URL(link);
                            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                            httpURLConnection.setRequestMethod("POST");
                            httpURLConnection.setDoOutput(true);
                            httpURLConnection.setDoInput(true);

                            OutputStream os = httpURLConnection.getOutputStream();

                            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

                            //transmission
                            bw.write(eDA);
                            bw.flush();
                            bw.write(eCounter);
                            bw.flush();
                            bw.write(eSessionID);
                            bw.flush();
                            bw.write(eUser);
                            bw.flush();
                            bw.close();
                            os.close();

                            InputStream is = httpURLConnection.getInputStream();
                            is.close();

                            httpURLConnection.disconnect();
                        }
                        break;
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }
    }
}
