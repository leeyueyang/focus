package com.app.lee.focus;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class Obstacles extends AppCompatActivity {

    String  Obstacle1 = "Ob1";
    String  Obstacle2 = "Ob2";
    String  Obstacle3 = "Ob3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obstacles);
        setup();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            dialogPrompt();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    private void dialogPrompt(){

        if(findViewString(R.id.obstacleTextEditOne).isEmpty() | findViewString(R.id.obstacleTextEditTwo).isEmpty() | findViewString(R.id.obstacleTextEditThree).isEmpty()){
            Toast.makeText(Obstacles.this, "Please fill in all the blanks", Toast.LENGTH_SHORT).show();
        }
        else {
            //creates dialog box
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);

            builder1.setMessage("Would you like to save these obstacles?");
            builder1.setCancelable(true);

            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                                try {
                                    saveData(findViewString(R.id.obstacleTextEditOne), Obstacle1);
                                    saveData(findViewString(R.id.obstacleTextEditTwo), Obstacle2);
                                    saveData(findViewString(R.id.obstacleTextEditThree), Obstacle3);

                                    startActivity(new Intent(getApplicationContext(), ifThenOne.class));
                                    finish();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                        }
                    });

            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            builder1.show();
        }
    }

    private String findViewString(int ID){
        TextView obstacleText = (TextView) findViewById(ID);
        String obstacle = obstacleText.getText().toString();
        return obstacle;
    }

    private void saveData(String data, String filename) throws IOException {
        OutputStreamWriter out = new OutputStreamWriter(openFileOutput(filename, MODE_PRIVATE));
        out.write(data);
        out.flush();
        out.close();
    }

    private void setup(){
        EditText addCourseText1 = (EditText) findViewById(R.id.obstacleTextEditOne);
        EditText addCourseText2 = (EditText) findViewById(R.id.obstacleTextEditTwo);
        EditText addCourseText3 = (EditText) findViewById(R.id.obstacleTextEditThree);
        addCourseText1.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {

                        case KeyEvent.KEYCODE_ENTER:
                            dialogPrompt();
                            return true;
                        default:
                            break;

                    }
                }
                return false;
            }
        });

        addCourseText2.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {

                        case KeyEvent.KEYCODE_ENTER:
                            dialogPrompt();
                            return true;
                        default:
                            break;

                    }
                }
                return false;
            }
        });

        addCourseText3.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {

                        case KeyEvent.KEYCODE_ENTER:
                            dialogPrompt();
                            return true;
                        default:
                            break;

                    }
                }
                return false;
            }
        });

    }
}
